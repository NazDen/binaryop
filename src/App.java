import java.util.Scanner;

public class App {
    public static void main(String[] args) {


        Scanner sc = new Scanner(System.in);
//      get decimal number
//      the input binary number length should be multiple 8 for correct subtraction
//      ex.( 00000001 )

//        System.out.println(sc.nextInt(2));

        char[] chars = sc.next().toCharArray();
        int numMaxI = chars.length - 1;
        int result = 0;
        for (int i = numMaxI; i >= 0; i--) {
            result = ((chars[i] & 1) << (numMaxI & (~i))) | result;
        }
        System.out.println(result);


////          get binary number
//        int x = sc.nextInt();
//        for (int i = 7; i >= 0; i--) {
////            int mask = 1 << i;
////           System.out.print((x & mask)>>i);
//            System.out.print((x >>i) & 1);
//        }
    }
}
